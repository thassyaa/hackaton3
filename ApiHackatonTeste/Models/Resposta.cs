﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiHackatonTeste.Models
{
    public class Resposta
    {
        public int QtdSeparada { get; set; }
        public decimal Porcentagem { get; set; }
        public int QtdPedidos { get; set; }
    }
}