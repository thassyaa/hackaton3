﻿using ApiHackatonTeste.Dao.DapperContext;
using ApiHackatonTeste.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;

namespace ApiHackatonTeste.Dao.StatusArea
{

    public class StatusAreaRepository
    {
        public string ConnectionString = ConfigurationManager.ConnectionStrings["Hackaton2018"].ConnectionString;

        public List<Resposta> GetLpn()
        {
            List<Resposta> lpnTeste = new List<Resposta>();

            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                var result = sqlConnection.Query<Resposta>(@"SELECT 
       ISNULL(SUM(PIV.quantidade), 0) as QtdSeparada, 
       CONVERT(MONEY, ROUND(SUM(ABS(PIV.Dif_CheckOut)) / SUM(PIV.quantidade) * 100.0, 2)) as Porcentagem ,
       COUNT (PED.cod_prenota) as QtdPedidos 

  FROM pedido PED (NOLOCK) INNER JOIN  
       pedido_item ITEM (NOLOCK) ON PED.cod_pedido = ITEM.cod_pedido INNER JOIN  
       pedido_item_lote_endereco PILE (NOLOCK) ON ITEM.cod_pedido = PILE.cod_pedido  
                                              AND ITEM.cod_produto = PILE.cod_produto INNER JOIN  
       pedido_item_volume PIV (NOLOCK) ON PILE.id_pedido_item_lote_endereco = PIV.id_pedido_item_lote_endereco INNER JOIN  
       pedido_volume  PV (NOLOCK) ON PED.cod_pedido = PV.cod_pedido  
 WHERE PED.operacao = 1 
   AND PED.cod_situacao IN (3, 4)
   AND PIV.conferido = -1  
   AND PV.Tipo_caixa IS NOT NULL  
   AND ISNULL(PIV.Dif_CheckOut, 0) <> 0  
   AND PIV.data_FimCheckout > = cast(cast(getdate() as date) as datetime)+cast(datepart(hour,getdate()) as float)/24
   AND PIV.data_FimCheckout < = DATEADD(HH, 1 , (cast(cast(getdate() as date) as datetime)+cast(datepart(hour,getdate()) as float)/24))
 GROUP BY PED.cod_prenota
HAVING ROUND(SUM(ABS(PIV.Dif_CheckOut)) / SUM(PIV.quantidade) * 100.0, 2) <= 100 -- Para eliminar picos muito altos ");

                foreach (var lpn in result)
                    lpnTeste.Add(lpn);
            }

            return lpnTeste;
        }
    }
}