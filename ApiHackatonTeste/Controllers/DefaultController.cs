﻿using ApiHackatonTeste.Dao.StatusArea;
using ApiHackatonTeste.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiHackatonTeste.Controllers
{
    public class DefaultController : ApiController
    {
        private readonly StatusAreaRepository _statusAreaRepository = new StatusAreaRepository();

        // GET: api/Default
        public List<Resposta> Get()
        {
            return _statusAreaRepository.GetLpn();
        }

        // GET: api/Default/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Default
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Default/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Default/5
        public void Delete(int id)
        {
        }
    }
}
