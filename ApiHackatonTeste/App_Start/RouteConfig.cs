﻿
using System.Web.Routing;

namespace ApiHackatonTeste.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            routes.MapPageRoute(routeName: "Default", routeUrl: "api/{controller}/{action}/{id}", physicalFile: "");
        }
    }
}